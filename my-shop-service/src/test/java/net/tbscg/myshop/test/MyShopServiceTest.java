package net.tbscg.myshop.test;

import net.tbscg.myshop.dao.model.*;
import net.tbscg.myshop.dao.service.IStorageService;
import net.tbscg.myshop.service.MyShopService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MyShopServiceTest {
    private MyShopService testedObject;
    @Mock
    private IStorageService storage;

    @Before
    public void setup(){
        testedObject = new MyShopService();
        testedObject.setStorageService(storage);
    }

    //Test Customer
    @Test
    public void testAddNewCustomer() throws Exception {
        Customer customer = new Customer("Test", "test1");
        when(storage.addCustomer(any())).thenReturn(customer);
        Customer customer2 = testedObject.addNewCustomer("Test", "test1");
        assertEquals(customer, customer2);
    }

    @Test
    public void testGetCustomerByName() throws Exception {
        Customer customer1 = new Customer("Test1", "test1");
        Customer customer2 = new Customer("Test2", "test2");
        when(storage.listCustomers()).thenReturn(Arrays.asList(customer1, customer2));
        Customer customerTaken = testedObject.getCustomerByName("Test2");
        assertEquals(customer2, customerTaken);


    }

    //Test Product
    @Test
    public void testAddRealProduct() throws Exception {
        Product realProduct1 = new RealProduct("Product1", new BigDecimal(123), "FajnyOpis", 1);
        when(storage.addProduct(any())).thenReturn(realProduct1);
        Product realProduct2 = testedObject.addRealProduct("Product1", new BigDecimal(123), "FajnyOpis", 1);
        assertEquals(realProduct1, realProduct2);
    }

    @Test
    public void testAddLicense() throws Exception {
        License licence = new License("New nice licence", new BigDecimal(123), "FajnyOpis");
        when(storage.addProduct(any())).thenReturn(licence);
        Product newLicense = testedObject.addLicence("New nice licence", new BigDecimal(123), "FajnyOpis");
        assertEquals(licence, newLicense);
    }

    @Test
    public void testListProducts() throws Exception {
        Product realProduct1 = new RealProduct("Product1", new BigDecimal(123), "FajnyOpis", 1);
        Product realProduct2 = new RealProduct("Product2", new BigDecimal(124), "FajnyOpis2", 1);
        List<Product> productList = new LinkedList<>();
        List<Product> productList2;
        productList.add(realProduct1);
        productList.add(realProduct2);
        when(storage.listProduct()).thenReturn(productList);
        productList2 = testedObject.listProducts();
        assertEquals(productList, productList2);
    }

    @Test
    public void testGetProductById() throws Exception {
        Product realProduct1 = new RealProduct("Product1", new BigDecimal(123), "FajnyOpis", 1);
        Product realProduct2 = new RealProduct("Product2", new BigDecimal(124), "FajnyOpis2", 1);
        List<Product> productList = new LinkedList<>();
        productList.add(realProduct1);
        productList.add(realProduct2);
        when(storage.getProductById(any())).thenReturn(realProduct1);
        assertEquals(realProduct1, testedObject.getProductById(1L));
    }

    @Test
    public void testMakeOrder() throws Exception {
        Product realProduct1 = new RealProduct("Product1", new BigDecimal(123), "FajnyOpis", 1);
        Customer customer1 = new Customer("Test1", "test1");
        Order newOrder = new Order(realProduct1, customer1, 2, new Date());
        when(storage.addOrder(any())).thenReturn(newOrder);
        assertEquals(newOrder, testedObject.makeOrder(realProduct1, customer1, 2));
    }
}
