package net.tbscg.myshop.service;

import net.tbscg.myshop.dao.model.*;
import net.tbscg.myshop.dao.service.IStorageService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class MyShopService implements IMyShopService {

    private IStorageService storageService;

    public void setStorageService(IStorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public Customer addNewCustomer(String userName, String address) throws IOException {
        Customer customer = new Customer(userName, address);
        storageService.addCustomer(customer);
        return customer;   }

    @Override
    public Customer getCustomerByName(String userName) {
        for(Customer customer:storageService.listCustomers()) {
           if (customer.getName().equals(userName)) {
                return customer;
           }
        }
        return null;
    }

    @Override
    public Product addRealProduct(String name, BigDecimal price, String desc, Integer qunatity) throws IOException {
        RealProduct realProduct = new RealProduct(name, price, desc, qunatity);
        storageService.addProduct(realProduct);
        return realProduct;
    }

    @Override
    public Product addLicence(String name, BigDecimal price, String desc) throws IOException {
        License license = new License(name, price, desc);
        storageService.addProduct(license);
        return license;
    }

    @Override
    public List<Product> listProducts() {
        return storageService.listProduct();
    }

    @Override
    public List<Customer> listCustomer() {
        return storageService.listCustomers();
    }

    @Override
    public List<Order> listOrder() {
        return storageService.listOrder();
    }

    @Override
    public Product getProductById(Long id) {
        return storageService.getProductById(id);
    }

    @Override
    public Order makeOrder(Product product, Customer customer, Integer quantity) throws IOException {
        Order order = new Order(product, customer, quantity, new Date());
        storageService.addOrder(order);
        return order;
    }

    @Override
    public Order getOrderById(Long id) {
        return storageService.getOrderById(id);

    }

    @Override
    public void deleteRealProduct(Long id) throws IOException {
        storageService.deleteProduct(storageService.getProductById(id));
    }

    @Override
    public void deleteCustomer(String id) throws IOException {
        storageService.deleteCustomer(storageService.getCustomerById(id));
    }

    @Override
    public void deleteOrder(Long id) throws IOException {
        storageService.deleteOrder(storageService.getOrderById(id));
    }

    @Override
    public void updateCustomer() throws IOException {
        storageService.updateCustomer();
    }

    @Override
    public void updateProduct() throws IOException {
        storageService.updateProduct();
    }

    @Override
    public void updateOrder() throws IOException {
        storageService.updateOrder();
    }
}
