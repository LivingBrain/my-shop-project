package net.tbscg.myshop.service;

import net.tbscg.myshop.dao.model.Customer;
import net.tbscg.myshop.dao.model.Order;
import net.tbscg.myshop.dao.model.Product;
import net.tbscg.myshop.dao.model.RealProduct;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;


public interface IMyShopService {

    Customer addNewCustomer(String userName, String address) throws IOException;
    Customer getCustomerByName(String userName);
    Product addRealProduct(String name, BigDecimal price, String desc, Integer quantity) throws IOException;
    Product addLicence(String name, BigDecimal price, String desc) throws IOException;
    List<Product> listProducts();
    List<Customer> listCustomer();
    List<Order> listOrder();
    Product getProductById(Long id);
    Order makeOrder(Product product, Customer customer, Integer quantity) throws IOException;
    Order getOrderById(Long id);
    void deleteRealProduct(Long id) throws IOException;
    void deleteCustomer(String id) throws IOException;
    void deleteOrder(Long id) throws IOException;

    void updateCustomer() throws IOException;
    void updateProduct() throws IOException;
    void updateOrder() throws IOException;
}
