package net.tbscg.myshop.dao.service;

import net.tbscg.myshop.dao.model.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.xml.sax.XMLReader;

import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

/**
 * Created by pborkowski on 2016-07-19.
 */
public class XMLFileStorageService extends StorageService {

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.0000");

    public XMLFileStorageService() throws IOException, ClassNotFoundException, DocumentException, ParseException {
        readFromFile();
    }

    public void saveToFile() throws IOException {
        File file = new File("db/storage.db");
        if (!file.exists()) {
            file.getParentFile();
            File dir = file.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            file.createNewFile();
        }

        Document doc = DocumentHelper.createDocument();
        Element rootElement = doc.addElement("Storage");
        Element productsElement = rootElement.addElement("Products");
        for (Product product : this.listProduct()) {
            if (product instanceof RealProduct) {
                RealProduct realProduct = (RealProduct) product;
                Element productElement = productsElement.addElement("realProduct");
                productElement.addAttribute("id", String.valueOf(product.getId()));
                productElement.addElement("Name").setText(product.getName());
//                productElement.addElement("Price").setText(DECIMAL_FORMAT.format(product.getPrice()));
                productElement.addElement("Quantity").setText(String.valueOf(((RealProduct) product).getQuantity()));
                productElement.addElement("Desc").setText(product.getDesc());
            } else if (product instanceof License) {
                License license = (License) product;
            }
        }
        Element customersElement = rootElement.addElement("Customers");
        for (Customer customer : this.listCustomers()) {
            Element customerElement = customersElement.addElement("Customer");
            customerElement.addAttribute("id", String.valueOf(customer.getId()));
            customerElement.addElement("Name").setText(customer.getName());
            customerElement.addElement("Address").setText(customer.getAddress());
        }
        Element ordersElement = rootElement.addElement("Orders");
        for (Order order : this.listOrder()) {
            Element orderElement = ordersElement.addElement("Order");
            orderElement.addAttribute("id", String.valueOf(order.getId()));
            Element orderCustomer = orderElement.addElement("Customer");
            orderCustomer.addAttribute("id", order.getCustomer().getId());

            orderElement.addElement("Product").setText(String.valueOf(order.getProduct().getId()));
        }

        try (FileOutputStream fos = new FileOutputStream(file)) {
            XMLWriter writer = new XMLWriter(fos);
            writer.write(doc);
        }


    }

    @Override
    public void updateCustomer() throws IOException {
        super.updateCustomer();
        saveToFile();
    }

    @Override
    public void updateProduct() throws IOException {
        super.updateProduct();
        saveToFile();
    }

    @Override
    public void updateOrder() throws IOException {
        super.updateOrder();
        saveToFile();
    }

    @SuppressWarnings("unchecked")
    private void readFromFile() throws IOException, ClassNotFoundException, DocumentException, ParseException {
        File file = new File("db/storage.db");
//        InputStream inputStream = new FileInputStream(file);
//        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        if (file.exists()) {
//            products = (List<Product>) objectInputStream.readObject();
//            customers = (List<Customer>) objectInputStream.readObject();
//            orders = (List<Order>) objectInputStream.readObject();
//            nextProductId = (Long) objectInputStream.readObject();
//            nextCustomerId = (Long) objectInputStream.readObject();
//            nextOrderId = (Long) objectInputStream.readObject();
            try (FileInputStream fis = new FileInputStream(file)) {
                SAXReader reader = new SAXReader();
                Document document = reader.read(fis);
                parseProducts(document);

            }
        }

    }

    private void parseProducts(Document document) throws ParseException, IOException {
        List<?> productNodes = document.selectNodes("//Storage/Products/realProduct");
        for (Object productNode : productNodes) {
            Element productElement = (Element) productNode;
            String id = productElement.valueOf("@id");
            String name = productElement.valueOf("Name");
            String priceString = productElement.valueOf("Price");
//            BigDecimal price = (BigDecimal) DECIMAL_FORMAT.parse(priceString);
            int quantity = Integer.parseInt(productElement.valueOf("Quantity"));
            String desc = productElement.valueOf("Desc");

            Product product = new RealProduct(Long.valueOf(id),name, null , desc, quantity);
            addProduct(product);
        }

//        List<?> licenseNodes = document.selectNodes("/Storage/Products/License");
    }

    @Override
    public Customer addCustomer(Customer customer) throws IOException {
        Customer addCustomer = super.addCustomer(customer);
        saveToFile();
        return addCustomer;
    }

    @Override
    public List<Customer> listCustomers() {
        return super.listCustomers();
    }

    @Override
    public Customer getCustomerById(String id) {
        return super.getCustomerById(id);
    }

    @Override
    public void deleteCustomer(Customer customer) throws IOException {
        super.deleteCustomer(customer);
        saveToFile();
    }

    @Override
    public Product addProduct(Product product) throws IOException {
        Product addProduct = super.addProduct(product);
        saveToFile();
        return addProduct;
    }

    @Override
    public List<Product> listProduct() {
        return super.listProduct();
    }

    @Override
    public Product getProductById(Long id) {
        return super.getProductById(id);
    }

    @Override
    public void deleteProduct(Product product) throws IOException {
        super.deleteProduct(product);
        saveToFile();
    }

    @Override
    public Order addOrder(Order order) throws IOException {
        Order addOrder = super.addOrder(order);
        saveToFile();
        return addOrder;
    }

    @Override
    public List<Order> listOrder() {
        return super.listOrder();
    }

    @Override
    public Order getOrderById(Long id) {
        return super.getOrderById(id);
    }

    @Override
    public void deleteOrder(Order order) throws IOException {
        super.deleteOrder(order);
        saveToFile();
    }
}
