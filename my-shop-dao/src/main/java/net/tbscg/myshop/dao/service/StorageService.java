package net.tbscg.myshop.dao.service;

import net.tbscg.myshop.dao.model.Customer;
import net.tbscg.myshop.dao.model.Order;
import net.tbscg.myshop.dao.model.Product;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class StorageService implements IStorageService {

    protected List<Customer> customers = new LinkedList<>();
    protected Long nextCustomerId = 1L;
    protected List<Product> products = new LinkedList<>();
    protected Long nextProductId = 1L;
    protected Long nextOrderId = 1L;
    protected List<Order> orders = new LinkedList<>();

    @Override
    public Customer addCustomer(Customer customer) throws IOException {
        customer.setId(String.valueOf(nextCustomerId));
        nextCustomerId++;
        customers.add(customer);
        return customer;
    }

    @Override
    public List<Customer> listCustomers() {
        return customers;
    }

    @Override
    public Customer getCustomerById(String id) {
        for (Customer customer:customers) {
            if (customer.getId().equals(id)) {
                return customer;
            }
        }
        return null;
    }

    @Override
    public void deleteCustomer(Customer customer) throws IOException {
        customers.remove(customer);
    }

    @Override
    public void updateCustomer() throws IOException {


    }

    //Products**********************
    @Override
    public Product addProduct(Product product) throws IOException {
        product.setId(nextProductId);
        nextProductId++;
        products.add(product);
        return product;
    }

    @Override
    public List<Product> listProduct() {
        return products;
    }

    @Override
    public Product getProductById(Long id) {
        for (Product product:products) {
            if (product.getId().equals(id)) {
                return product;
            }
        }
        return null;
    }

    @Override
    public void deleteProduct(Product product) throws IOException {
        products.remove(product);
    }

    @Override
    public void updateProduct() throws IOException {

    }

    //Orders***********************
    @Override
    public Order addOrder(Order order) throws IOException {
        order.setId(nextOrderId);
        nextOrderId++;
        orders.add(order);
        return order;
    }

    @Override
    public List<Order> listOrder() {
        return orders;
    }

    @Override
    public Order getOrderById(Long id) {
        for (Order order:orders) {
            if (order.getId().equals(id)) {
                return order;
            }
        }
        return null;
    }

    @Override
    public void deleteOrder(Order order) throws IOException {
        orders.remove(order);
    }

    @Override
    public void updateOrder() throws IOException {

    }
}
