package net.tbscg.myshop.dao.service;

import net.tbscg.myshop.dao.model.Customer;
import net.tbscg.myshop.dao.model.Order;
import net.tbscg.myshop.dao.model.Product;

import java.io.*;
import java.util.List;

/**
 * Created by pborkowski on 2016-03-15.
 */
public class FileStorageService extends StorageService {

    public FileStorageService() throws IOException, ClassNotFoundException {
        readFromFile();
    }

    public void saveToFile() throws IOException {
        File file = new File("db/storage.db");
        if (!file.exists()) {
            file.getParentFile();
            File dir = file.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            file.createNewFile();
        }

        OutputStream outputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(listProduct());
        objectOutputStream.writeObject(listCustomers());
        objectOutputStream.writeObject(listOrder());
        objectOutputStream.writeObject(nextProductId);
        objectOutputStream.writeObject(nextCustomerId);
        objectOutputStream.writeObject(nextOrderId);
    }

    @Override
    public void updateCustomer() throws IOException {
        super.updateCustomer();
        saveToFile();
    }

    @Override
    public void updateProduct() throws IOException {
        super.updateProduct();
        saveToFile();
    }

    @Override
    public void updateOrder() throws IOException {
        super.updateOrder();
        saveToFile();
    }

    @SuppressWarnings("unchecked")
    public void readFromFile() throws IOException, ClassNotFoundException {
        File file = new File("db/storage.db");
        InputStream inputStream = new FileInputStream(file);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        if (file.exists()) {
            products = (List<Product>) objectInputStream.readObject();
            customers = (List<Customer>) objectInputStream.readObject();
            orders = (List<Order>) objectInputStream.readObject();
            nextProductId = (Long) objectInputStream.readObject();
            nextCustomerId = (Long) objectInputStream.readObject();
            nextOrderId = (Long) objectInputStream.readObject();
        }
    }

    @Override
    public Customer addCustomer(Customer customer) throws IOException {
        Customer addCustomer = super.addCustomer(customer);
        saveToFile();
        return addCustomer;
    }

    @Override
    public List<Customer> listCustomers() {
        return super.listCustomers();
    }

    @Override
    public Customer getCustomerById(String id) {
        return super.getCustomerById(id);
    }

    @Override
    public void deleteCustomer(Customer customer) throws IOException {
        super.deleteCustomer(customer);
        saveToFile();
    }

    @Override
    public Product addProduct(Product product) throws IOException {
        Product addProduct = super.addProduct(product);
        saveToFile();
        return addProduct;
    }

    @Override
    public List<Product> listProduct() {
        return super.listProduct();
    }

    @Override
    public Product getProductById(Long id) {
        return super.getProductById(id);
    }

    @Override
    public void deleteProduct(Product product) throws IOException {
        super.deleteProduct(product);
        saveToFile();
    }

    @Override
    public Order addOrder(Order order) throws IOException {
        Order addOrder = super.addOrder(order);
        saveToFile();
        return addOrder;
    }

    @Override
    public List<Order> listOrder() {
        return super.listOrder();
    }

    @Override
    public Order getOrderById(Long id) {
        return super.getOrderById(id);
    }

    @Override
    public void deleteOrder(Order order) throws IOException {
        super.deleteOrder(order);
        saveToFile();
    }
}
