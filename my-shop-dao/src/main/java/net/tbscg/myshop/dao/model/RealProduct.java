package net.tbscg.myshop.dao.model;

import java.math.BigDecimal;

/**
 * Created by pborkowski on 2015-12-15.
 */
public class RealProduct extends Product {

    protected Integer quantity;

    public RealProduct(Long id, String name, BigDecimal price, String desc, Integer quantity) {
        super(name, price, desc);
        this.quantity = quantity;
    }

    public RealProduct(String name, BigDecimal price, String desc, Integer quantity) {
        super(name, price, desc);
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {

        return
                super.toString() +
                "RealProduct{" +
                "quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RealProduct that = (RealProduct) o;

        return !(quantity != null ? !quantity.equals(that.quantity) : that.quantity != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        return result;
    }
}
