package net.tbscg.myshop.dao.service;

import net.tbscg.myshop.dao.model.Customer;
import net.tbscg.myshop.dao.model.Order;
import net.tbscg.myshop.dao.model.Product;

import java.io.IOException;
import java.util.List;

/**
 * Created by pborkowski on 2016-01-21.
 */
public interface IStorageService {

    Customer addCustomer(Customer customer) throws IOException;
    List<Customer> listCustomers();
    Customer getCustomerById(String Id);
    void deleteCustomer(Customer customer) throws IOException;
    void updateCustomer() throws IOException;

    Product addProduct(Product product) throws IOException;
    List<Product> listProduct();
    Product getProductById(Long id);
    void deleteProduct(Product product) throws IOException;
    void updateProduct() throws IOException;

    Order addOrder(Order order) throws IOException;
    List<Order> listOrder();
    Order getOrderById(Long id);
    void deleteOrder(Order order) throws IOException;
    void updateOrder() throws IOException;

}
