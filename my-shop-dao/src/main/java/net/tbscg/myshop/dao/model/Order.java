package net.tbscg.myshop.dao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by pborkowski on 2015-12-15.
 */
public class Order implements Serializable {

    private Product product;
    private Customer customer;
    private Integer quantity;
    private Date date;
    private Long id;

    public Order(Long id, Product product, Customer customer, Integer quantity, Date date) {
        this.product = product;
        this.customer = customer;
        this.quantity = quantity;
        this.date = date;
        this.id = id;
    }

    public Order(Product product, Customer customer, Integer quantity, Date date) {
        this.product = product;
        this.customer = customer;
        this.quantity = quantity;
        this.date = date;
    }

    public Product getProduct() {
        return product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Date getDate() {
        return date;
    }

    public Long getId() {
        return id;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "product=" + product +
                ", customer=" + customer +
                ", quantity=" + quantity +
                ", date=" + date +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (product != null ? !product.equals(order.product) : order.product != null) return false;
        if (customer != null ? !customer.equals(order.customer) : order.customer != null) return false;
        if (quantity != null ? !quantity.equals(order.quantity) : order.quantity != null) return false;
        return !(id != null ? !id.equals(order.id) : order.id != null);

    }

    @Override
    public int hashCode() {
        int result = product != null ? product.hashCode() : 0;
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
