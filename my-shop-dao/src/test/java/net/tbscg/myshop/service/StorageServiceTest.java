package net.tbscg.myshop.service;

import net.tbscg.myshop.dao.model.Customer;
import net.tbscg.myshop.dao.model.Order;
import net.tbscg.myshop.dao.model.Product;
import net.tbscg.myshop.dao.model.RealProduct;
import net.tbscg.myshop.dao.service.StorageService;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class StorageServiceTest {
    private StorageService testedObject; //Ustawiamy jako pole klase testowana

    @Before
    public void setup() {
        this.testedObject = new StorageService();
    }

    //Test Customer
    @Test
    public void testAddCustomer() throws Exception{
        Customer c = new Customer("Pawel","Plac Solny");
        testedObject.addCustomer(c);
        assertEquals(1, testedObject.listCustomers().size());
        assertEquals(c.getName(), testedObject.listCustomers().get(0).getName());
        assertEquals("1", testedObject.listCustomers().get(0).getId());

    }

    @Test
    public void testListCustomers() throws Exception{
        Customer c1 = new Customer("Test Customer1", "Gdzies1");
        Customer c2 = new Customer("Test Customer2", "Gdzies2");
        testedObject.addCustomer(c1);
        testedObject.addCustomer(c2);
        List<Customer> customersNewList = testedObject.listCustomers();
        assertEquals(2, customersNewList.size());
    }

    @Test
    public void testGetCustomerById() throws Exception{
        Customer c1 = new Customer("Test Customer1", "Gdzies1");
        Customer c2 = new Customer("Test Customer2", "Gdzies2");
        testedObject.addCustomer(c1);
        testedObject.addCustomer(c2);
        assertNotEquals(c1, testedObject.getCustomerById("2"));
        assertEquals(c1, testedObject.getCustomerById("1"));
    }

    @Test
    public void testDeleteCustomer() throws IOException {
        Customer c1 = new Customer("Test Customer1", "Gdzies1");
        testedObject.addCustomer(c1);
        String getCustomerID = c1.getId();
        testedObject.deleteCustomer(c1);
        assertNotEquals(c1, testedObject.getCustomerById(getCustomerID));
    }

    //Test Product
    @Test
    public void testAddProduct() throws Exception{
        RealProduct realProduct = new RealProduct(1L, "Product", new BigDecimal(123), "FajnyOpis", 2);
        testedObject.addProduct(realProduct);
        assertEquals(1, testedObject.listProduct().size());
        assertEquals(realProduct.getName(), testedObject.listProduct().get(0).getName());
        assertEquals("1", String.valueOf(testedObject.listProduct().get(0).getId()));

    }

    @Test
    public void testListProducts() throws Exception{
        Product product1 = new Product("Product1", new BigDecimal(123), "FajnyOpis") {};
        Product product2 = new Product("Product2", new BigDecimal(4), "FajnyOpis2") {};
        testedObject.addProduct(product1);
        testedObject.addProduct(product2);
        List<Product> productsNewList = testedObject.listProduct();
        assertEquals(2, productsNewList.size());
    }

    @Test
    public void testGetProductById() throws Exception{
        Product product1 = new Product("Product1", new BigDecimal(123), "FajnyOpis") {};
        Product product2 = new Product("Product2", new BigDecimal(4), "FajnyOpis2") {};
        testedObject.addProduct(product1);
        testedObject.addProduct(product2);
        assertNotEquals(product1, testedObject.getProductById(2L));
        assertEquals(product2, testedObject.getProductById(2L));
    }

    @Test
    public void testDeleteProduct() throws IOException {
        Product product1 = new Product("Product1", new BigDecimal(123), "FajnyOpis") {};
        testedObject.addProduct(product1);
        Long getProductId = product1.getId();
        testedObject.deleteProduct(product1);
        assertNotEquals(product1, testedObject.getProductById(getProductId));
    }

    //Test Order
    @Test
    public void testAddOrder() throws IOException {
        Order order = new Order(1L, new Product("Product", new BigDecimal(123), "FajnyOpis") {}, new Customer("Pawel","Plac Solny"), 1, new Date());
        testedObject.addOrder(order);
        assertEquals(1, testedObject.listOrder().size());
        assertEquals(order.getCustomer(), testedObject.listOrder().get(0).getCustomer());
    }

    @Test
    public void testListOrders() throws IOException {
        Order order = new Order(1L, new Product("Product", new BigDecimal(123), "FajnyOpis") {}, new Customer("Pawel","Plac Solny"), 1, new Date());
        testedObject.addOrder(order);
        List<Order> ordersNewList = testedObject.listOrder();
        assertEquals(1, ordersNewList.size());
        assertEquals(ordersNewList, testedObject.listOrder());
    }

    @Test
    public void testGetOrderById() throws IOException {
        Customer customer1 = new Customer("Pawel","Plac Solny");
        Customer customer2 = new Customer("Pawel2","Plac Solny2");
        Product product1 = new Product("Product1", new BigDecimal(123), "FajnyOpis") {};
        Product product2 = new Product("Product2", new BigDecimal(4), "FajnyOpis2") {};
        Order order = new Order(1L, product1, customer1, 1, new Date());
        Order order2 = new Order(2L, product2, customer2, 3, new Date());
        testedObject.addOrder(order);
        testedObject.addOrder(order2);
        System.out.println(order2);
        assertNotEquals(order, testedObject.getOrderById(2L));
        assertEquals(order2, testedObject.getOrderById(2L));
    }

    @Test
    public void testDeleteOrder() throws IOException {
        Customer customer1 = new Customer("Pawel","Plac Solny");
        Product product1 = new Product("Product1", new BigDecimal(123), "FajnyOpis") {};
        Order order = new Order(1L, product1, customer1, 1, new Date());
        testedObject.addOrder(order);
        testedObject.deleteOrder(order);
        assertNotEquals(order, testedObject.getOrderById(1L));
    }
}
