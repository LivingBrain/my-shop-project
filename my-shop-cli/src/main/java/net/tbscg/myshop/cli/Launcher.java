package net.tbscg.myshop.cli;

import net.tbscg.myshop.dao.model.Customer;
import net.tbscg.myshop.dao.model.Order;
import net.tbscg.myshop.dao.model.Product;
import net.tbscg.myshop.dao.model.RealProduct;
import net.tbscg.myshop.dao.service.FileStorageService;
import net.tbscg.myshop.dao.service.XMLFileStorageService;
import net.tbscg.myshop.service.MyShopService;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Launcher extends Menus {

    private static MyShopService myShopService;

    public static void main(String[] args) throws IOException, ClassNotFoundException, DocumentException, ParseException {
        System.out.println("App is working!");
        myShopService = new MyShopService();
        myShopService.setStorageService(new XMLFileStorageService());
        //myShopService.addRealProduct("Product1", new BigDecimal(123), "FajnyOpis", 2);
//        myShopService.addNewCustomer("Customer1", "Test address");
//        Product realProduct = myShopService.addRealProduct("Product2", new BigDecimal(123), "FajnyOpis", 6);
//        Customer customer = myShopService.addNewCustomer("Customer2", "Test address2");
//        myShopService.makeOrder(realProduct, customer, 3);
        mainMenu2();

//        int[] tab = new int[13];43
//        for(int x = 0; x < 13; x++ ){
//            if (x == tab.length / 2 || x == tab.length / 2 - 1 || x == tab.length / 2 + 1 ) {
//                tab[x] = 2;
//            } else {
//                tab[x] = 1;
//            }
//        }
//        System.out.println(Arrays.toString(tab));
//        System.out.println(tab.length / 2);
    }

    public static String printMenu(List<String> menuList, String operation){
        for(String menuPosition: menuList) {
            System.out.println(menuPosition);
        }
        Scanner keyboard = new Scanner(System.in);
        if (operation.equals("")) {
            System.out.print("Pick menu option: ");
        } else {
            System.out.print("Do you want to " + operation + "? (y)Yes (n)No: ");
        }
        return keyboard.nextLine();
    }

    public static void printProduct(Product product) {
        RealProduct realProduct = (RealProduct) product;
        System.out.println("Product id:           " + product.getId());
        System.out.println("Product name:         " + product.getName());
        System.out.println("Product price:        " + product.getPrice());
        System.out.println("Product description:  " + product.getDesc());
        System.out.println("Product quantity:     " + realProduct.getQuantity());
        System.out.println("");
    }

    public static void printCustomer(Customer customer) {
        System.out.println("Customer id:          " + customer.getId());
        System.out.println("Customer name:        " + customer.getName());
        System.out.println("Customer address:     " + customer.getAddress());
        System.out.println("");
    }

    public  static void printOrder(Order order) {
        System.out.println("Order's id:           " + order.getId());
        System.out.println("Order's date:         " + order.getDate());
        System.out.println("Order's quantity:     " + order.getQuantity());
        System.out.println("Order's customer:");
        printCustomer(order.getCustomer());
        System.out.println("Order's product:");
        printProduct(order.getProduct());
    }

    public static void mainMenu2() throws IOException {
        Boolean ifLoop = true;
        while (ifLoop) {
            String menuItem = printMenu(mainMenuList, "");
            if (menuItem.equals("1")) {
                firstMenu();
            } else if (menuItem.equals("2")) {
                secondMenu();
            } else if (menuItem.equals("3")) {
                thirdMenu();
            } else if (menuItem.equals("4")) {
                System.exit(0);
            } else {
                System.out.println("Wrong menu item. Please use correct menu option.");
            }
        }
    }

    //1
    public static void firstMenu() throws IOException {
        Boolean ifLoop = true;
        while (ifLoop) {
            String menuItem = printMenu(firstMenuList, "");
            if (menuItem.equals("1.1")) {
                firstFirstMenu();
            } else if (menuItem.equals("1.2")) {
                firstSecondMenu();
            } else if (menuItem.equals("1.3")) {
                firstThirdMenu();
            } else if (menuItem.equals("1.4")) {
                firstFourthMenu();
            } else if (menuItem.equals("1.5")) {
                mainMenu2();
            } else {
                System.out.println("Wrong menu item. Please use correct menu option.");
            }
        }
    }

    public static String addNewProduct() throws IOException {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Product name: ");
        String nazwaProducktu = keyboard.nextLine();
        System.out.print("Product price: ");
        String wartoscProducktu = keyboard.nextLine();
        System.out.print("Product description: ");
        String opisProduktu = keyboard.nextLine();
        System.out.print("Product quantity: ");
        Integer iloscProduktu = keyboard.nextInt();
        myShopService.addRealProduct(nazwaProducktu, new BigDecimal(wartoscProducktu), opisProduktu, iloscProduktu);
        return nazwaProducktu;
    }

    //1.1
    public static void firstFirstMenu() throws IOException {
        String menuOption = printMenu(subMenuList("1.1 Add product"), "add new product");
        if (menuOption.equals("y")) {
            System.out.println("New Product: " + addNewProduct() + " was added to the database.");
            firstMenu();
        } else if (menuOption.equals("n")) {
            firstMenu();
        }
    }

    //1.2
    public static void firstSecondMenu() throws IOException {
        String menuOption = printMenu(subMenuList("1.2 Delete product"), "delete product");
        if (menuOption.equals("y")) {
            Scanner keyboard = new Scanner(System.in);
            System.out.print("Type in product id: ");
            myShopService.deleteRealProduct(keyboard.nextLong());
            firstMenu();
        } else if (menuOption.equals("n")) {
            firstMenu();
        }
    }

    //1.3
    public static void firstThirdMenu() throws IOException {
        String menuOption = printMenu(subMenuList("1.3 Edit produkt"), "edit product");
        if (menuOption.equals("y")) {
            Scanner keyboard = new Scanner(System.in);

            System.out.print("Type in productu id: ");
            Product productToEdit = myShopService.getProductById(Long.parseLong(keyboard.nextLine()));
            RealProduct realProductToEdit = (RealProduct) productToEdit;
            System.out.println("Product name: " + productToEdit.getName());
            System.out.print("New product name: ");
            productToEdit.setName(keyboard.nextLine());
            System.out.println("Product price: " + productToEdit.getPrice());
            System.out.print("New product price: ");
            productToEdit.setPrice(new BigDecimal(keyboard.nextLine()));
            System.out.println("Product description: " + productToEdit.getDesc());
            System.out.print("New product description: ");
            productToEdit.setDesc(keyboard.nextLine());
            System.out.println("Product quantity: " + realProductToEdit.getQuantity());
            System.out.print("New product quantity: ");
            myShopService.updateProduct();
            realProductToEdit.setQuantity(keyboard.nextInt());
            firstMenu();
        } else if (menuOption.equals("n")) {
            firstMenu();
        }
    }

    //1.4
    public static void firstFourthMenu() throws IOException {
        String menuOption = printMenu(subMenuList("1.4 Products list"), "list products");
        if (menuOption.equals("y")) {
            for (Product product:myShopService.listProducts() ) {
                printProduct(product);
            }
            firstMenu();
        } else if (menuOption.equals("n")) {
            firstMenu();
        }
    }

    //2
    public static void secondMenu() throws IOException {
        Boolean ifLoop = true;
        while (ifLoop) {
            String menuItem = printMenu(secondMenuList, "");
            if (menuItem.equals("2.1")) {
                secondFirstMenu();
            } else if (menuItem.equals("2.2")) {
                secondSecondMenu();
            } else if (menuItem.equals("2.3")) {
                secondThirdMenu();
            } else if (menuItem.equals("2.4")) {
                secondFourthMenu();
            } else if (menuItem.equals("2.5")) {
                mainMenu2();
            } else {
                System.out.println("Wrong menu item. Please use correct menu option.");
            }
        }
    }


    public static String addNewCustomer() throws IOException {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Customer name: ");
        String customerName = keyboard.nextLine();
        System.out.print("Customer address: ");
        String customerAddress = keyboard.nextLine();
        myShopService.addNewCustomer(customerName, customerAddress);
        return customerName;
    }

    //2.1
    public static void secondFirstMenu() throws IOException {
        String menuOption = printMenu(subMenuList("2.1 Add client"), "add new customer");
        if (menuOption.equals("y")) {
            System.out.println("New Customer: " + addNewCustomer() + " was added to the database.");
            secondMenu();
        } else if (menuOption.equals("n")) {
            secondMenu();
        }
    }

    //2.2
    public static void secondSecondMenu() throws IOException {
        String menuOption = printMenu(subMenuList("2.2 Delete client"), "delete customer");
        if (menuOption.equals("y")) {
            Scanner keyboard = new Scanner(System.in);
            System.out.print("Type in customer name: ");
            myShopService.deleteCustomer(keyboard.nextLine());
            secondMenu();
        } else if (menuOption.equals("n")) {
            secondMenu();
        }
    }

    //2.3
    public static void secondThirdMenu() throws IOException {
        String menuOption = printMenu(subMenuList("2.3 Edit client"), "edit customer");
        if (menuOption.equals("y")) {
            Scanner keyboard = new Scanner(System.in);

            System.out.print("Type in customer name: ");
            Customer customerToEdit = myShopService.getCustomerByName(keyboard.nextLine());
            System.out.println("Customer name: " + customerToEdit.getName());
            System.out.print("New customer name: ");
            customerToEdit.setName(keyboard.nextLine());
            System.out.println("Customer address: " + customerToEdit.getAddress());
            System.out.print("New customer address: ");
            customerToEdit.setAddress(keyboard.nextLine());
            secondMenu();
        } else if (menuOption.equals("n")) {
            secondMenu();
        }
    }

    //2.4
    public static void secondFourthMenu() throws IOException {
        String menuOption = printMenu(subMenuList("2.4 Clients list"), "list customers");
        if (menuOption.equals("y")) {
            for (Customer customer:myShopService.listCustomer() ) {
                printCustomer(customer);
            }
            secondMenu();
        } else if (menuOption.equals("n")) {
            secondMenu();
        }
    }




    //3
    public static void thirdMenu() throws IOException {
        Boolean ifLoop = true;
        while (ifLoop) {
            String menuItem = printMenu(thirdMenuList, "");
            if (menuItem.equals("3.1")) {
                thirdFirstMenu();
            } else if (menuItem.equals("3.2")) {
                thirdSecondMenu();
            } else if (menuItem.equals("3.3")) {
                thirdThirdMenu();
            } else if (menuItem.equals("3.4")) {
                thirdFourthMenu();
            } else if (menuItem.equals("3.5")) {
                mainMenu2();
            } else {
                System.out.println("Wrong menu item. Please use correct menu option.");
            }
        }
    }


    //3.1
    public static void thirdFirstMenu() throws IOException {
        String menuOption = printMenu(subMenuList("3.1 Add order"), "make new order");
        if (menuOption.equals("y")) {
            Scanner keyboard = new Scanner(System.in);
            System.out.print("Type in product id: ");
            Product realProduct = myShopService.getProductById(Long.parseLong(keyboard.nextLine()));
            System.out.print("Type in customer name: ");
            Customer customer = myShopService.getCustomerByName(keyboard.nextLine());
            System.out.print("Type in order quantity: ");
            String orderQuantity = keyboard.nextLine();
            myShopService.makeOrder(realProduct, customer, Integer.parseInt(orderQuantity));
            System.out.println("New Order: was added to the database.");
            thirdMenu();
        } else if (menuOption.equals("n")) {
            thirdMenu();
        }
    }

    //3.2
    public static void thirdSecondMenu() throws IOException {
        String menuOption = printMenu(subMenuList("3.2 Delete order"), "delete order");
        if (menuOption.equals("y")) {
            Scanner keyboard = new Scanner(System.in);
            System.out.print("Type in order id: ");
            myShopService.deleteOrder(Long.parseLong(keyboard.nextLine()));
            thirdMenu();
        } else if (menuOption.equals("n")) {
            thirdMenu();
        }
    }

    //3.3
    public static void thirdThirdMenu() throws IOException {
        String menuOption = printMenu(subMenuList("3.3 Edit order"), "edit order");
        if (menuOption.equals("y")) {
            Scanner keyboard = new Scanner(System.in);
            System.out.print("Type in order id: ");
            Order orderToEdit = myShopService.getOrderById(Long.parseLong(keyboard.nextLine()));
            System.out.println("Order's customer name: " + orderToEdit.getCustomer());
            System.out.print("Type in new order's customer name: ");
            orderToEdit.setCustomer(myShopService.getCustomerByName(keyboard.nextLine()));
            System.out.print("Type in new order's product id: ");
            orderToEdit.setProduct(myShopService.getProductById(Long.parseLong(keyboard.nextLine())));
            System.out.print("Type in new order's quantity: ");
            orderToEdit.setQuantity(Integer.parseInt(keyboard.nextLine()));
            thirdMenu();
        } else if (menuOption.equals("n")) {
            thirdMenu();
        }
}

    //3.4
    public static void thirdFourthMenu() throws IOException {
        String menuOption = printMenu(subMenuList("3.4 Order list"), "list orders");
        if (menuOption.equals("y")) {
            for (Order order:myShopService.listOrder() ) {
                printOrder(order);
            }
            thirdMenu();
        } else if (menuOption.equals("n")) {
            thirdMenu();
        }
    }

}
