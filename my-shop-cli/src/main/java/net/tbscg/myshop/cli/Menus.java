package net.tbscg.myshop.cli;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Menus {
    public static final List<String> mainMenuList = new LinkedList<>(Arrays.asList(
            "***************************",
            "*        ===Menu===       *",
            "*1. Products Management   *",
            "*2. Clients Management    *",
            "*3. Orders Management     *",
            "*4. Exit                  *",
            "***************************"
    ));

    public static final List<String> firstMenuList = new LinkedList<>(Arrays.asList(
            "******************************",
            "*** 1. Products Management ***",
            "       1.1 Add product",
            "       1.2 Delete product",
            "       1.3 Edit product",
            "       1.4 List products",
            "       1.5 Return",
            "******************************"
    ));

    public static final List<String> secondMenuList = new LinkedList<>(Arrays.asList(
            "******************************",
            "*** 2. Clients Management ***",
            "       2.1 Add client",
            "       2.2 Delete client",
            "       2.3 Edit client",
            "       2.4 List clients",
            "       2.5 Return",
            "******************************"
    ));

    public static final List<String> thirdMenuList = new LinkedList<>(Arrays.asList(
            "******************************",
            "*** 3. Orders Management ***",
            "       3.1 Add order",
            "       3.2 Delete order",
            "       3.3 Edit order",
            "       3.4 List orders",
            "       3.5 Return",
            "******************************"
    ));

    public static List<String> subMenuList(String menuItem) {
        return new LinkedList<>(Arrays.asList(
                "*************************",
                "*** " + menuItem + " ***"
        ));
    }
}
